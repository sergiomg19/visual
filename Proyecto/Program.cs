﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones operacion = new Operaciones(4, 2);
            double resultadoS = operacion.suma();
            Console.WriteLine("El resultado de la suma es " +  resultadoS);
            double resultadoD = operacion.div();
            operacion.div();
            Console.WriteLine("El resultado de la division es " + resultadoD);
            double resultadoM = operacion.multi();
            operacion.multi();
            Console.WriteLine("El resultado de la multiplicacion es " + resultadoM);
            double resultadoR = operacion.resta();
            operacion.resta();
            Console.WriteLine("El resultado de la resta es " + resultadoR);

        }
    }
}
